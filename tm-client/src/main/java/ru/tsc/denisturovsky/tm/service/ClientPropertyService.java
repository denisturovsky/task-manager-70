package ru.tsc.denisturovsky.tm.service;

import lombok.Getter;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.PropertySource;
import org.springframework.stereotype.Service;
import ru.tsc.denisturovsky.tm.api.service.IClientPropertyService;

@Getter
@Service
@PropertySource("classpath:application.properties")
public class ClientPropertyService implements IClientPropertyService {

    @Value("#{environment['buildNumber']}")
    private String applicationVersion;

    @Value("#{environment['server.port']}")
    private String port;

    @Value("#{environment['server.host']}")
    private String host;

    @Value("#{environment['admin.login']}")
    private String adminLogin;

    @Value("#{environment['admin.password']}")
    private String adminPassword;

}

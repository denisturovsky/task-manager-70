package ru.tsc.denisturovsky.tm;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.component.ServerBootstrap;

import static ru.tsc.denisturovsky.tm.context.ServerContext.CONTEXT;


public final class Application {

    public static void main(@Nullable String[] args) {
        @NotNull final ServerBootstrap serverBootstrap = CONTEXT.getBean(ServerBootstrap.class);
        serverBootstrap.run();
    }

}

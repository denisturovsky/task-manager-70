package ru.tsc.denisturovsky.tm.api.service.dto;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.tsc.denisturovsky.tm.dto.model.AbstractUserOwnedModelDTO;
import ru.tsc.denisturovsky.tm.enumerated.CustomSort;

import java.util.List;

public interface IUserOwnedDTOService<M extends AbstractUserOwnedModelDTO> extends IDTOService<M> {

    @NotNull
    M add(
            @Nullable String userId,
            @NotNull M model
    ) throws Exception;

    void clear(@Nullable String userId) throws Exception;

    @Nullable
    List<M> findAll(
            @Nullable String userId,
            @Nullable CustomSort sort
    ) throws Exception;

    @Nullable
    List<M> findAll(@Nullable String userId) throws Exception;

    @Nullable
    M findOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    int getSize(@Nullable String userId) throws Exception;

    void remove(
            @Nullable String userId,
            @Nullable M model
    ) throws Exception;

    void removeOneById(
            @Nullable String userId,
            @Nullable String id
    ) throws Exception;

    void update(
            @Nullable String userId,
            @Nullable M model
    ) throws Exception;

}
